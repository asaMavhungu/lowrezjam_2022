use gdnative::{
	api::{AStar2D, Time},
	prelude::*,
};

#[derive(NativeClass)]
#[inherit(Node2D)]
struct NavigateTwo {
	#[property]
	astar: Ref<AStar2D>,
	#[property]
	astar_debug: Option<Ref<Node>>,
	_generated_size: Vector2,
}

#[methods]
impl NavigateTwo {
	fn new(_owner: &Node2D) -> Self {
		NavigateTwo {
			astar: AStar2D::new().into_shared(),
			astar_debug: None,
			_generated_size: Vector2::default(),
		}
	}

	#[method]
	fn _ready(&mut self, #[base] base: TRef<Node2D>) {
		self.astar_debug = Some(base.get_node("AStar2DDebug").unwrap())
	}

	#[method]
	fn generate(&mut self, size: Vector2, water_data: VariantArray) {
		self._generated_size = size;
		let astar = unsafe { self.astar.assume_safe() };
		let size_x = size.x as i32;
		let size_y = size.y as i32;
		astar.clear();
		astar.reserve_space((size_x * size_y) as i64);
		let tile_size = 6;
		let tile_size_offset = tile_size / 2;
		let mut y = tile_size_offset;
		while y < size_y {
			let mut x = tile_size_offset;
			while x < size_x {
				let i = y * size_x + x;
				if water_data.get(i).try_to::<f32>().unwrap() < 0.75 * 255.0 {
					let i = i as i64;
					let position = Vector2::new(x as f32, y as f32);
					astar.add_point(i as i64, position, 1.0);
					for u in -1..=1 {
						for v in -1..=1 {
							if u == 0 && v == 0 {
								continue;
							}
							let neighbor_id =
								((y + u * tile_size) * size_x + (x + v * tile_size)) as i64;
							if astar.has_point(neighbor_id as i64) {
								astar.connect_points(i, neighbor_id, true)
							}
						}
					}
				}
				x += tile_size;
			}
			y += tile_size;
		}
	}

	#[method]
	fn enable_rect(&self, rect: Rect2, enable: bool) {
		let astar = unsafe { self.astar.assume_safe() };
		for y in (rect.position.y as i32)..(rect.end().y as i32) {
			for x in (rect.position.x as i32)..(rect.end().x as i32) {
				let i = (y * (self._generated_size.x as i32) + x) as i64;
				if astar.has_point(i) {
					astar.set_point_disabled(i, !enable);
				}
			}
		}
	}

	#[method]
	fn find_path(&self, from: Vector2, to: Vector2) -> Vector2Array {
		let t = Time::godot_singleton().get_ticks_usec();
		let astar = unsafe { self.astar.assume_safe() };
		// NOTE(david): build path
		let from_id = astar.get_closest_point(from, false);
		let to_id = astar.get_closest_point(to, false);
		let mut path = Vector2Array::new();
		path.push(from);
		path.append(&astar.get_point_path(from_id, to_id));
		// NOTE(david): avoid small backtrack
		if path.len() > 2 {
			let from_to_second = (from - path.get(2)).length();
			let first_to_second = (path.get(1) - path.get(2)).length();
			if from_to_second < first_to_second {
				path.remove(1);
			}
		}
		// NOTE(david): avoid straying too far into unpathable terrain
		let distance_to_target = (to - astar.get_point_position(to_id)).length();
		let mut enabled_connection_count = 0;
		for &conn_id in astar.get_point_connections(to_id).to_vec().iter() {
			if !astar.is_point_disabled(conn_id as i64) {
				enabled_connection_count += 1;
			}
		}
		if enabled_connection_count == 8 || distance_to_target <= 3.0 {
			path.push(to);
		}
		godot_print!("{}", Time::godot_singleton().get_ticks_usec() - t);
		return path;
	}
}

fn init(handle: InitHandle) {
	handle.add_class::<NavigateTwo>();
}

godot_init!(init);
