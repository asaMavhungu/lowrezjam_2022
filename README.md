# LOWREZJAM 2022

## Resources

- https://godotengine.org/
- https://videodante.itch.io/tinypixel
- https://youtu.be/zxVQsi9wnw8
- https://code-disaster.com/2016/02/subpixel-perfect-smooth-scrolling.html
- https://github.com/GDQuest/godot-2d-space-game

wtf just like a whole commercial Godot game lol:
- https://github.com/HarmonyHoney/ROTA

## Things

state:
- resource (wood)

world:
- trees
- water
- building
- unit

actions:
- camera move
- building placement 
- building selection
- building create units
- unit selection
- unit drag selection
- units move
- units harvest
- units attack

ai:
- pathfinding
- decision tree

## TODO

- [x] generate world data
    - world size (192x192) (3x3 x 64x64)
    - layered smooth noise
        - water
        - trees
    - nagivation grid (tile size 4x4?) - what are the options?
        - godot's AStar2D class: https://docs.godotengine.org/en/stable/classes/class_astar2d.html
            - performance might be bad, but should be pretty easy to modify at runtime
        - generate my own polygons somehow and add to: https://docs.godotengine.org/en/stable/classes/class_navigation2d.html
            - difficult to generate, difficult to update, but probably good runtime perf
        - use a tilemap just for nagivation: https://youtu.be/5xoqp9QMKeg
            - easy to generate, easy to update? but probably bad runtime perf
        - custom A* over world data
        - switch to godot 3.5 and use the new NavigationServer2D
- [x] render some sprites and what not
    - trees with some jitter :)
    - water:
        - edge line (animated?)
        - beach waves
        - large body waves
        - can use the noise gradient to outline edges and help with animation
- [x] camera movement (try and smooth it out later)
- [x] building placement
    - check surroundings for water, trees, and buildings
- [x] building selection
    - check for click on building scene??
    - set state to BUILDING_SELECTED
    - animate open the create unit panel
    - selection visuals??
        - pops out in front with a frame behind it (use a viewport?)
    - click away deselects
- [x] building create units
    - visualise creation progress?
    - visualise creation queue?
- [x] unit selection
    - visualise selection?
        - https://youtu.be/x6dOxJa3zXY
        - https://youtu.be/sZlX9o9uF7o
        - basically create a full screen texture rect
        - loop through units and change the color of pixels around them on the texture
            - OR! blur it in the shader (with textureLod function?)
        - in the shader check neighbouring uv coords to highlight the edge of the selection
    - ok ok I think I have the node tree needed:
        - CanvasLayer
            - ViewportContainer with stretch
                - Viewport
                    - Camera that matches main camera's transform
                    - ...render objects here that need outlines
- [x] show building waypoint when selected
- [x] update building sprite to be more primitive
- [x] shift+click unit button to queue max
- [x] add cursor sprites:
    - !!! okay i think i should just have a system which tells me what the mouse is hovering at any time
        - `Physics2DDirectSpaceState.intersect_point()` for 2D world
        - `mouse_entered`/`mouse_exited` signals for UI
    - use hardware cusor
    - animation is possible, but requires manual updates
    - use different cursor "shapes" for UI only
        - easily changed in Control insepector
    - game states control the default cursor shape
        - might just need to reset the default cursor when hovering UI
        - could check on every frame, or mouse move, or mouse enter UI
    - cursor states:
        - hovering ui button -> pointer
        - hovering ui -> arrow
        - idle -> arrow
        - placing building -> hammer
        - units selected:
            - hovering pathable terrain -> move indicator
            - hovering nonpathable terrain -> prohibited indicator
            - hovering tree -> harvest indicator (axe?)
            - hovering enemy -> attack indicator (swords?)
        - building selected:
            - hovering pathable terrain -> move indicator (sets waypoint)
            - hovering nonpathable terrain -> prohibited indicator (sets waypoint to nearest pathable?)
            - hovering tree -> harvest indicator (units auto harvest on build??)
- [x] ~~convert all PNG to WebP~~ (importing webps from aseprite is a bit buggy??)
- [x] use a flag as the waypoint
- [x] animate hammer cursor
- [x] feedback for commanding units to move: show a waypoint for a bit?
    - kinda works, but could be better 
- [x] update selection box visuals (can i do an dotted outline in a shader?)
- [x] UI
  - [x] wood counter
  - [x] hover state
- [x] ~~add white outline to cursors??~~ (it's a bit too much for this resolution)
- [x] unit AI
    - [x] experiment using coroutines for ai decision "tree"
        - need to be able to switch tasks per frame, can yield control back and call resume each frame
        - yeah so it works pretty well using coroutines and action lists as described here: https://eliasdaler.github.io/how-to-implement-action-sequences-and-cutscenes/
    - behaviours:
        - [x] move to point
        - [x] harvest trees
            - told to harvest specific tree:
                - go to tree
                - harvest tree (handle if already harvested)
                - go to nearest house
                - drop off wood
                - go back to last harvest location
                - search for nearby tree
                - repeat until no more nearby tree
        - [x] deposit wood if got
        - etc. (attack enemy once enemies are in the game)
    - [x] when navigation grid is updated, units jitter... if distance from position to second node is less than distance from first to second node, skip first?? (yes indeed)
- [x] sfx
    - [x] build
    - [x] click btn_build
    - [x] click btn_unit
    - [x] click btn_unit when queue full
    - [x] spawn unit
    - [x] cut tree
    - [x] deposit wood
    - [x] select building
    - [x] select unit
    - [x] set waypoint
    - [x] command unit
- [x] improve tree cutting:
    - [x] leave tree stumps on ground when tree cut
    - [x] give tree health so it takes time to cut, e.g., 3 health, 1 hit/s, takes 3 seconds to cut
- [x] reorganise project/files/code/assets/etc., it's a bit too messy. 
    - what are like the main modules that i can make global singletons of??
        - Global (something?)
        - Events
        - Map/Terrain
            - World is already a Godot class
            - I think most things will depend on this
        - UI
        - Navigate
            - Navigation is already a class in Godot
        - Selection
        - Building
        - Unit
        - Tree/Resources
        - SFX
        - Cursor
    - okay... here's the main issue, gdscript does not support **_cyclic references_** in any shape or form (as of 23/10/22), which means it is **impossible** for me to organise code how I want to, and reduces the effectiveness of static typing. I simply want a collection of globally available modules that get intialised at runtime to handle the bulk of detailed logic, and static typing. This is basically my game's engine layer on top of godot, then there will just be a few game scripts that tie it together. For these reasons, I think gdscript might be on the way out, and after looking at C, C++, C#, and Rust, Rust appears as the most appealing option. While this is a deal-breaker for gdscript, I will probably still use it for general game logic, because coroutines are \*chef's kiss\*.
    - right, let's make a new branch and attempt an engine layer in rust!
    - while rust/gdnative does have its advantages, if I still want to do scripting in gdscript, it doesn't solve the referencing issues, but just forces me not to have them as I would not be able to reference my own gdscript classes in rust. still, doing this was a valuable exercise as it shows that I can continue with gdscript, but I must keep track of the dependency graph, which will probably be easier with clearly defined modules as autoload singletons.
    - Another thing I think might be best is to keep my entities like unit, building, and tree, completely methodless; just data.
    - At the moment, I'm just randomly moving stuff around to see what feels good, and abstracting away dependencies between modules.
    - I think I want `game.gd` to simply orchestrate what happens by calling specific modules that deal with the details; world gen, navigation, events, etc.
    - right, so to make coroutines work for the main game "state machine" in a "nice" way; any next state has to be set from within the coroutine, so it knows when to `break`, so that the exit code can run before the next start; or, I have to add an explicit "exit" resume message, which is gross.
        - okay, okay, coroutines can't solve all my problems, the FSM I have for this works much nicer, so I will keep that :)
- [x] right click btn_unit dequeues unit
- [x] show waypoint for unit selection. can keep track of commanded target pos of unit and loop through on selection collecting the different positions if any to show zero or more waypoints
- [x] unit collision avoidance:
    - when units overlap, try move them away from eachother
    - moving units have priority, so won't move away from idle units
    - debounce the collision code?
    - really trying not to use dynamics here...
    - this might mess up the formations :/
- [x] building: set waypoint on tree will make spawned units go there and cut wood
- [x] ~~use pixels for wood counter?? (like unit build queue progress)~~
- [x] experiment with Fog of War:
    - completely black for undiscovered terrain, dithered at the edges
    - make a world sized viewport and use a screen effect to draw the visible area
        - has to be a screen effect so different visibility values can be drawn and dithered all at once
        - should be able to find a nice dither shader online, acerola perhaps?
        - https://en.wikipedia.org/wiki/Ordered_dithering
        - each frame i would update a screen sized texture with the value of the (screen) visible areas of terrain
        - every so often (start with per frame but probably more like 1s or 0.5s) loop through buildings/units to update explorered/visible terrain data
        - add some warbley noise to the fog?? could be a bit much as this resolution
    - each unit/building has a linked node with a shader that draws a circle (gradient?) in a separate viewport, the viewport runs a dither shader and is then composited over the world
    - is it possible to limit the FPS of a viewport?
        - you can set the render target update mode and do it manually I suppose
- [x] variable resolution
- [x] ~~can the selection graphics be done almost entirely with VisualServer~~
    - I suppose it technically can, but only in a way that's more complicated than nodes.
- [x] experiment with xBR scaling (sokpop effect)
    - https://en.wikipedia.org/wiki/Pixel-art_scaling_algorithms
    - managed to get it working! but it would require ensuring everything is pixelated and locked to the grid before rendering, as a post processing effect.
    - it could be applied per entity for more control
    - it doesn't really work well for this game, but definitely handy to have in the bag for future stylized work :)
- [ ] improve water shader
- [ ] surround map with water, making an island
- [ ] make a generic Rectangle2D shape node that's editable in the scene view
- [ ] "discovered" fog of war
    - dithered for discovered but not visible
    - I think I would need two textures (viewports) to have discovered AND visible. Discovered has to persist the data drawn to it, while visible is cleared each frame.
    - with data persistence I'll have to handle alpha values differently so they don't summate and create harsh dither transitions
    - this also might just look awful and be ostensibly useless lol
