class_name BtnUnit
extends TextureButton

onready var queue := $Queue.material as ShaderMaterial
onready var progress := $Progress.material as ShaderMaterial

var _building: Building = null

var _tween: SceneTreeTween = null

func _ready():
	self.disabled = true
	self.connect("mouse_entered", self, "_on_mouse_entered")
	self.connect("mouse_exited", self, "_on_mouse_exited")
	Events.connect("building_selected", self, "_on_building_selected")
	Events.connect("building_deselected", self, "_on_building_deselected")


func _process(_delta):
	if _building != null:
		queue.set_shader_param("amount", _building.get_queue_size())
		progress.set_shader_param("progress", _building.get_build_progress())


func _on_mouse_entered():
	if !self.disabled:
		queue.set_shader_param("inverted", true)
		progress.set_shader_param("inverted", true)


func _on_mouse_exited():
	queue.set_shader_param("inverted", false)
	progress.set_shader_param("inverted", false)


func _on_building_selected(building: Building):
	self.disabled = false
	_building = building
	if _tween != null: _tween.kill()
	_tween = get_tree().create_tween()
	_tween.tween_property(self, "rect_position:x", 11.0, 0.3)\
		.set_trans(Tween.TRANS_CUBIC)\
		.set_ease(Tween.EASE_OUT)


func _on_building_deselected(__building: Building):
	self.disabled = true
	_building = null
	if _tween != null: _tween.kill()
	_tween = get_tree().create_tween()
	_tween.tween_property(self, "rect_position:x", 1.0, 0.3)\
		.set_trans(Tween.TRANS_CUBIC)\
		.set_ease(Tween.EASE_OUT)\
		.set_delay(0.1)
