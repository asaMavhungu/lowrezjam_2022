extends Control

func _ready():
	_connect_signals(self)


func _connect_signals(control: Control):
	for child in control.get_children():
		if child is Control:
			_connect_signals(child)
			child.connect("mouse_entered", self, "_on_mouse_entered", [child])
			child.connect("mouse_exited", self, "_on_mouse_exited", [child])


func _on_mouse_entered(control: Control):
	Global.ui_over = control


func _on_mouse_exited(control: Control):
	if Global.ui_over == control:
		Global.ui_over = null
