extends Sprite

var size := Vector2(192, 192)

var water_noise := OpenSimplexNoise.new()
var water_data := PoolByteArray()
var water_image := Image.new()
var water_tex := ImageTexture.new()
var water_level: int = 191

var tree_noise := OpenSimplexNoise.new()
var tree_jitter: int = 2
var tree_positions := PoolVector2Array()


func _init():
	water_noise.octaves = 1
	water_noise.period = 30
	tree_noise.octaves = 1
	tree_noise.period = 30
	self.texture = water_tex


func generate():
	var s := size

	# NOTE(david): generate water data
	water_noise.seed = randi()
	water_data.resize(s.x * s.y)
	for y in range(s.y):
		for x in range(s.x):
			var i := y * s.x + x
			var val := (water_noise.get_noise_2d(x, y) + 1.0) / 2.0
			water_data[i] = val * 255
	water_image.create_from_data(s.x, s.y, false, Image.FORMAT_R8, water_data)
	water_tex.create_from_image(water_image)

	# NOTE(david): generate trees
	for n in get_tree().get_nodes_in_group("trees"):
		n.get_parent().remove_child(n)
		n.queue_free()
	tree_noise.seed = randi()
	var t_ts := 6 # tile size
	# warning-ignore:integer_division
	var t_ts_offset := t_ts / 2
	for y in range(t_ts_offset, s.y, t_ts):
		for x in range(t_ts_offset, s.x, t_ts):
			var i := y * s.x + x
			if tree_noise.get_noise_2d(x, y) > 0.5 and water_data[i] < water_level:
				tree_positions.push_back(Vector2(x, y))
