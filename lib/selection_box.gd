extends Sprite

onready var mat := self.material as ShaderMaterial

var _start: Vector2
var _end: Vector2


func _draw():
	position = _start
	scale = (_end - _start)
	scale.x = ceil(scale.x) if scale.x > 0 else floor(scale.x)
	scale.y = ceil(scale.y) if scale.y > 0 else floor(scale.y)
	mat.set_shader_param("sprite_scale", self.scale)


func set_selection(start: Vector2, end: Vector2):
	_start = start
	_end = end
	update()
