class_name Unit
extends Area2D

var commanded_target: bool = false
var commanded_target_pos: Vector2

var _speed: float = 10.0
var _carry_speed: float = 5.0

var _path: Array = []
var _debug_nav := false
var _last_moved: int = 0
var _avoid_co: GDScriptFunctionState = null

var _actions := []
var _action_co: GDScriptFunctionState = null
var _action = null

var _got_wood := false
var _last_harvest_position: Vector2

onready var _visuals := $Visuals
var _selected := false

onready var _sprite := $Visuals/Sprite
onready var _sprite_carry := $Visuals/SpriteCarry

onready var _audio := $AudioStreamPlayer2D
var _sfx_cut_wood := preload("res://lib/sfx/cut_wood.wav")
var _sfx_deposit_wood := preload("res://lib/sfx/deposit_wood.wav")

var _fow_circle: FogOfWarCircle = null

class ActionGoTo:
	var position: Vector2

class ActionCutTree:
	var tree: MyTree

class ActionDepositWood:
	var building: Building

class ActionWait:
	var duration: float

class ActionClearCommandedTarget:
	pass


func _ready():
	self.connect("area_entered", self, "_on_area_entered")
	Events.connect("navigation_updated", self, "_on_navigation_updated")
	set_debug_nav(Global.debug_view == Global.DebugView.UNIT_NAV)
	add_to_group("units")
	_fow_circle = FogOfWar.create_circle(32)


func _physics_process(delta):
	if _avoid_co is GDScriptFunctionState and _avoid_co.is_valid():
		_avoid_co = _avoid_co.resume(delta)
	elif _action_co is GDScriptFunctionState and _action_co.is_valid():
		_action_co = _action_co.resume(delta)
	else:
		_action = _actions.pop_front()
		if _action is ActionGoTo:
			_action_co = _go_to_routine(_action.position).resume(delta)
		elif _action is ActionCutTree:
			_action_co = _cut_tree_routine(_action.tree).resume(delta)
		elif _action is ActionDepositWood:
			_action_co = _deposit_wood_routine(_action.building).resume(delta)
		elif _action is ActionWait:
			_action_co = _wait_routine(_action.duration).resume(delta)
		elif _action is ActionClearCommandedTarget:
			commanded_target = false

	if _debug_nav: update()


func _process(_delta):
	_fow_circle.global_position = self.global_position
	_sprite.visible = !_got_wood
	_sprite_carry.visible = _got_wood
	if _selected:
		_visuals.global_position = self.global_position


func _draw():
	if _debug_nav:
		for i in range(0, _path.size() - 1):
			draw_line(_path[i] - position, _path[i+1] - position, Color.darkorchid, 1.0)


func set_debug_nav(val: bool):
	_debug_nav = val


func select():
	_selected = true
	Selection.select(_visuals)


func deselect():
	_visuals.get_parent().remove_child(_visuals)
	self.add_child(_visuals)
	_visuals.set_owner(self)
	_visuals.position = Vector2.ZERO
	_selected = false


func is_avoiding():
	return _avoid_co is GDScriptFunctionState and _avoid_co.is_valid()


func clear_actions():
	_actions.clear()
	_action_co = null


func command_go_to(target: Vector2, offset: Vector2 = Vector2(0, 0)):
	clear_actions()
	commanded_target = true
	commanded_target_pos = target
	_go_to(target + offset)
	_actions.push_back(ActionClearCommandedTarget.new())


func command_harvest(tree: MyTree):
	clear_actions()
	_go_to(tree.position + _rand_point_on_circle(4))
	_cut_tree(tree)


func command_harvest_at(target: Vector2):
	clear_actions()
	_go_to(target)
	_cut_tree(null)


func command_deposit_wood(building: Building):
	clear_actions()
	_go_to(building.position)
	if _got_wood:
		_deposit_wood(building)


func _go_to(target: Vector2):
	var go_to := ActionGoTo.new()
	go_to.position = target
	_actions.push_back(go_to)


func _cut_tree(tree: MyTree):
	var cut_tree := ActionCutTree.new()
	cut_tree.tree = tree
	_actions.push_back(cut_tree)


func _deposit_wood(building: Building):
	var deposit_wood := ActionDepositWood.new()
	deposit_wood.building = building
	_actions.push_back(deposit_wood)


func _wait(duration: float):
	var wait := ActionWait.new()
	wait.duration = duration
	_actions.push_back(wait)


func _go_to_routine(target: Vector2):
	_path = Navigate.find_path(position, target)
	if _path.size() == 0:
		return yield()
	for i in _path.size():
		var from: Vector2 = position if i == 0 else _path[i - 1]
		var to: Vector2 = _path[i]
		var weight := 0.0
		while true:
			var delta: float = yield()
			_last_moved = Time.get_ticks_msec()
			var dist := (to - from).length() + 0.00001
			var speed := _carry_speed if _got_wood else _speed
			weight = clamp(weight + delta * (speed / dist), 0.0, 1.0)
			position = lerp(from, to, weight)
			if position == to:
				break
	_path.clear()


func _cut_tree_routine(tree: MyTree):
	yield()
	if tree is MyTree:
		if !_got_wood:
			tree.health -= 1
			_audio.stream = _sfx_cut_wood
			_audio.pitch_scale = rand_range(0.9, 1.1)
			_audio.play()
			if tree.health <= 0:
				Global.game.remove_tree(tree)
				_got_wood = true
			else:
				_wait(1.0)
				_cut_tree(tree)
		_last_harvest_position = tree.position
		if _got_wood:
			var building = Global.game.find_nearest_building(position)
			if building != null:
				_go_to(building.position)
				_deposit_wood(building)
	else:
		tree = Global.game.find_nearby_tree(position, 32.0)
		# TODO: if tree == null: find nearby tree from nearby building or last deposit location?
		if tree != null:
			_go_to(tree.position + _rand_point_on_circle(4))
			_cut_tree(tree)


func _deposit_wood_routine(building: Building):
	yield()
	if building is Building:
		Global.game.update_wood(1)
		_got_wood = false
		_audio.stream = _sfx_deposit_wood
		_audio.play()
		var tree = Global.game.find_nearby_tree(_last_harvest_position, 32.0)
		if tree == null:
			tree = Global.game.find_nearby_tree(building.position, 32.0)
		if tree != null:
			_go_to(tree.position + _rand_point_on_circle(4))
			_cut_tree(tree)
		else:
			_go_to(_last_harvest_position)
	else:
		building = Global.game.find_nearest_building(position)
		if building != null:
			_go_to(building.position)
			_deposit_wood(building)


func _wait_routine(duration: float):
	var timer: float = 0.0
	while true:
		var delta: float = yield()
		timer += delta
		if timer >= duration:
			break


func _on_navigation_updated():
	if _action is ActionGoTo:
		_action_co = _go_to_routine(_action.position) as GDScriptFunctionState


func _on_area_entered(other: Area2D):
	# NOTE: other should be Unit due to mask
	var moved_recently := Time.get_ticks_msec() - _last_moved < 2_000
	if !moved_recently and !other.is_avoiding():
		var dir = (self.position - other.position).normalized()
		_avoid_co = _go_to_routine(self.position + dir * 2)


# source: https://stackoverflow.com/a/50746409
func _rand_point_in_circle(radius: float) -> Vector2:
	var r = radius * sqrt(randf())
	return _rand_point_on_circle(r)


func _rand_point_on_circle(radius: float) -> Vector2:
	var theta = randf() * TAU
	var x = radius * cos(theta)
	var y = radius * sin(theta)
	return Vector2(x, y)
