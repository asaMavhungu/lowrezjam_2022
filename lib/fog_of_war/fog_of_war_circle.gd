class_name FogOfWarCircle
extends Sprite


func set_radius(radius: float):
	var d := radius * 2
	scale.x = d
	scale.y = d
