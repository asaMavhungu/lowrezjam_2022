extends CanvasLayer

onready var _viewport: Viewport = $Viewport
onready var _vis_contributors := $Viewport/VisibilityContributors

var _fog_of_war_circle := preload("res://lib/fog_of_war/fog_of_war_circle.tscn")

var _texture: ViewportTexture
var _data: Image

var _update_sec: float = 0.25
var _update_timer: float = 0.0
var _update_data_co: GDScriptFunctionState


func _ready():
	_viewport.render_target_update_mode = Viewport.UPDATE_ONCE
	_texture = _viewport.get_texture()
	_update_data_co = _update_data_routine()


func _physics_process(delta):
	if _update_data_co is GDScriptFunctionState and _update_data_co.is_valid():
		_update_data_co = _update_data_co.resume()
	_update_timer += delta
	if _update_timer > _update_sec:
		_update_timer -= _update_sec
		_viewport.render_target_update_mode = Viewport.UPDATE_ONCE
		_update_data_co = _update_data_routine()


func set_size(size: Vector2):
	_viewport.size = size


func get_texture():
	return _texture


func create_circle(radius: float):
	var fow_vis: FogOfWarCircle = _fog_of_war_circle.instance()
	fow_vis.set_radius(radius)
	_vis_contributors.add_child(fow_vis)
	return fow_vis


func is_point_visible(point: Vector2):
	if Rect2(Vector2.ZERO, _viewport.size).has_point(point):
		return _data.get_pixelv(point).r > 0.25
	return false


func _update_data_routine():
	yield()
	_data = _texture.get_data()
	_data.lock()
