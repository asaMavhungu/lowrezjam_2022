class_name Game
extends Node2D

onready var camera := $"%Camera"

onready var entities_box := $Entities

onready var building_placeholder := $"%BuildingPlaceholder"

var unit_scene := preload("res://lib/unit/unit.tscn")

var tree_scene := preload("res://lib/tree/tree.tscn")
var tree_stump_scene := preload("res://lib/tree/tree_stump.tscn")

var building_scene := preload("res://lib/building/building.tscn")

var can_select := true
onready var selection_box := $"%SelectionBox"

onready var btn_gen := $"%BtnGen"
onready var btn_build := $"%BtnBuild"
onready var btn_unit := $"%BtnUnit"
onready var lbl_wood := $"%LblWood"

var wood: int = 0
var building_cost: int = 1
var unit_cost: int = 1

onready var fsm := $"%FSM"

onready var _audio := $AudioStreamPlayer
var _sfx_click_btn_build := preload("res://lib/sfx/click_btn_build.wav")
var _sfx_select_building := preload("res://lib/sfx/select_building.wav")
var _sfx_select_unit := preload("res://lib/sfx/select_unit.wav")

var _co: GDScriptFunctionState = null


# Called when the node enters the scene tree for the first time.
func _ready():
	Global.game = self

	btn_gen.connect("pressed", self, "_on_BtnGen_pressed")
	btn_build.connect("pressed", self, "_on_BtnBuild_pressed")
	Events.connect("wood_updated", self, "_on_wood_updated")

	set_wood(30)

	VisualServer.set_default_clear_color(Color.black)

#	Map.size = Vector2(1024, 1024)
	Map.generate()
	for pos in Map.tree_positions:
		var tree: MyTree = tree_scene.instance()
		tree.position = pos
		entities_box.add_child(tree)
	Navigate.generate(Map.size, Map.water_data, Map.water_level)
	for tree in get_tree().get_nodes_in_group("trees"):
		Navigate.enable_rect(tree.navigation_rect, false)
	Events.emit_signal("navigation_updated")
	Selection.sync_with_camera(camera)
	Selection.set_deselect_node(entities_box)
	FogOfWar.set_size(Map.size)
	$FogOfWarSprite.texture = FogOfWar.get_texture()


func _unhandled_input(event):
	if _co is GDScriptFunctionState and _co.is_valid():
		_co = _co.resume(event)
	elif can_select and event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		get_tree().set_input_as_handled()
		_co = _selection_routine() as GDScriptFunctionState
	_handle_debug(event)


func _process(_delta):
	_update_world_over()
	_update_cursor()
	Global.visible_over = FogOfWar.is_point_visible(get_global_mouse_position().floor())


func _physics_process(delta):
	if _co is GDScriptFunctionState and _co.is_valid():
		_co = _co.resume(delta)


func remove_tree(tree: MyTree):
	Navigate.enable_rect(tree.navigation_rect, true)
	var tree_stump := tree_stump_scene.instance()
	tree_stump.position = tree.global_position + tree.jitter
	entities_box.add_child(tree_stump)
	entities_box.remove_child(tree)
	tree.queue_free()
	Events.emit_signal("navigation_updated")


func place_building(pos: Vector2):
	update_wood(-building_cost)
	var building: Building = building_scene.instance()
	building.position = pos
	entities_box.add_child(building)
	Navigate.enable_rect(building.navigation_rect, false)
	Events.emit_signal("navigation_updated")


func find_nearest_building(point: Vector2) -> Building:
	var buildings: Array = get_tree().get_nodes_in_group("buildings")
	var nearest: Building = buildings.pop_back()
	var nearest_score: int = Navigate.find_path(point, nearest.position).size() if nearest != null else 9223372036854775807
	for builing in buildings:
		var score = Navigate.find_path(point, builing.position).size()
		if score < nearest_score:
			nearest_score = score
			nearest = builing
	return nearest


func find_nearby_tree(point: Vector2, radius: float) -> MyTree:
	var shape := CircleShape2D.new()
	shape.radius = radius
	var shape_query := Physics2DShapeQueryParameters.new()
	shape_query.collision_layer = 2
	shape_query.set_shape(shape)
	shape_query.collide_with_bodies = false
	shape_query.collide_with_areas = true
	shape_query.transform.origin = point
	var space_state := get_world_2d().direct_space_state
	var intersections := space_state.intersect_shape(shape_query, 64)
	var trees := []
	for inter in intersections:
		if inter.collider is MyTree:
			trees.push_back(inter.collider)
	return trees[randi() % trees.size()] if trees.size() > 0 else null


func update_wood(amount: int):
	var old = wood
	wood = clamp(wood + amount, 0, 99)
	Events.emit_signal("wood_updated", wood, old)


func set_wood(value: int):
	var old = wood
	wood = clamp(value, 0, 99)
	Events.emit_signal("wood_updated", wood, old)


func spawn_unit(pos: Vector2) -> Unit:
	var unit: Unit = unit_scene.instance()
	unit.position = pos
	entities_box.add_child(unit)
	unit.set_owner(entities_box)
	return unit


func play_sfx(stream: AudioStream):
	_audio.stream = stream
	_audio.play()


func _reparent(child: Node, new_parent: Node):
	child.get_parent().remove_child(child)
	new_parent.add_child(child)
	child.set_owner(new_parent)


func _selection_routine():
	var start = get_global_mouse_position().floor()
	selection_box.set_selection(start, start)
	selection_box.visible = true
	while true:
		# NOTE: input could be delta or an unhandled event
		var input = yield()
		if input is InputEventMouseMotion:
			selection_box.set_selection(start, get_global_mouse_position().floor())
		elif input is InputEventMouseButton and input.button_index == BUTTON_LEFT and !input.pressed:
			get_tree().set_input_as_handled()
			var end := get_global_mouse_position().floor()
			var shape := RectangleShape2D.new()
			shape.extents = (end - start) / 2.0
			var shape_query := Physics2DShapeQueryParameters.new()
			shape_query.set_shape(shape)
			# NOTE(david): buildings and units
			shape_query.collision_layer = 4 + 8
			shape_query.collide_with_areas = true
			shape_query.collide_with_bodies = true
			shape_query.transform.origin = start + shape.extents
			var space_state := get_world_2d().direct_space_state
			var intersections := space_state.intersect_shape(shape_query, 64)
			var units := []
			var buildings := []
			for inter in intersections:
				if inter.collider is Unit:
					units.push_back(inter.collider)
				elif inter.collider is Building:
					buildings.push_back(inter.collider)
			if units.size() > 0:
				fsm.transition_to("UnitsSelected", { units = units })
			elif buildings.size() > 0:
				fsm.transition_to("BuildingSelected", { building = buildings[0] })
			break
	selection_box.visible = false


func _handle_debug(event: InputEvent):
	if event is InputEventKey:
		if event.scancode == KEY_F3 and event.pressed:
			Global.debug_view = (Global.debug_view + 1) % Global.DebugView.size()
			Navigate.astar_debug.visible = Global.debug_view == Global.DebugView.ASTAR
			get_tree().call_group("units", "set_debug_nav", Global.debug_view == Global.DebugView.UNIT_NAV)


func _update_cursor():
	var ui_over = Global.ui_over
	if ui_over is BaseButton and not ui_over.disabled:
		Global.cursor = Global.Cursor.POINTER
	elif ui_over is Control:
		Global.cursor = Global.Cursor.ARROW

	match Global.cursor:
		Global.Cursor.ARROW:
			Input.set_custom_mouse_cursor(preload("res://lib/ui/cursors/cursor_arrow.png"))
		Global.Cursor.POINTER:
			Input.set_custom_mouse_cursor(preload("res://lib/ui/cursors/cursor_pointer.png"), Input.CURSOR_ARROW, Vector2(32, 0))
		Global.Cursor.AXE:
			Input.set_custom_mouse_cursor(preload("res://lib/ui/cursors/cursor_axe.png"), Input.CURSOR_ARROW, Vector2(32, 16))
		Global.Cursor.HAMMER:
			var hammer1 = preload("res://lib/ui/cursors/cursor_hammer1.png")
			var hammer2 = preload("res://lib/ui/cursors/cursor_hammer2.png")
			var cursor = hammer1 if sin(Time.get_ticks_msec() / 100.0) > 0 else hammer2
			Input.set_custom_mouse_cursor(cursor, Input.CURSOR_ARROW, Vector2(64, 0))
		Global.Cursor.FORBID:
			Input.set_custom_mouse_cursor(preload("res://lib/ui/cursors/cursor_forbid.png"), Input.CURSOR_ARROW, Vector2(48, 48))


func _update_world_over():
	Global.world_over = null
	if Global.ui_over != null or not Global.visible_over:
		return
	var space_state := get_world_2d().direct_space_state
	var mouse_pos = get_global_mouse_position()
	var intersections := space_state.intersect_point(mouse_pos, 1, [], 0x7FFFFFFF, true, true)
	for inter in intersections:
		Global.world_over = inter.collider


func _on_BtnGen_pressed():
	Map.generate()


func _on_BtnBuild_pressed():
	fsm.transition_to("PlacingBuilding")
	play_sfx(_sfx_click_btn_build)


func _on_wood_updated(new: int, _old: int):
	lbl_wood.text = str(new)
