class_name BuildingPlaceholder
extends Sprite

var intersecting := false

var _shape_query := Physics2DShapeQueryParameters.new()


func _ready():
	disable()
	_shape_query.set_shape($PhysicsQueryShape.shape)
	_shape_query.collide_with_areas = true
	_shape_query.collide_with_bodies = false
	# NOTE(david): trees and buildings
	_shape_query.collision_layer = 2 + 4


func _physics_process(_delta):
	intersecting = false
	position = get_global_mouse_position().floor()
	# NOTE: query map and water
	var extents: Vector2 = $MapQueryShape.shape.extents
	var rect = Rect2($MapQueryShape.global_position - extents, 2 * extents)
	for y in range(rect.position.y, rect.end.y):
		for x in range(rect.position.x, rect.end.x):
			if x < 0 or x >= Map.size.x or y < 0 or y >= Map.size.y:
				intersecting = true
			elif Map.water_data[y * Map.size.x + x] > 0.75 * 255:
				intersecting = true
	if !intersecting:
		var space_state := get_world_2d().direct_space_state
		var shape_pos := position + Vector2(0, -2.5)
		_shape_query.transform.origin = shape_pos
		var intersections := space_state.intersect_shape(_shape_query, 1)
		intersecting = intersections.size() > 0


func enable():
	visible = true
	set_physics_process(true)


func disable():
	visible = false
	set_physics_process(false)
