class_name Building
extends Area2D

var unit_build_time: float = 4.0

onready var waypoint = $Waypoint

var navigation_rect: Rect2

var _co: GDScriptFunctionState
var _queue_limit := 5
var _build_queue := []
var _progress: float = 0.0;

var _harvest_on_spawn := false

onready var _smoke = $Sprite/Smoke
onready var _audio = $AudioStreamPlayer2D

var _sfx_build = preload("res://lib/sfx/build.wav")
var _sfx_spawn_unit = preload("res://lib/sfx/spawn_unit.wav")

var _fow_circle: FogOfWarCircle = null


func _ready():
	var pos: Vector2 = $NavigationRect.global_position
	var extents: Vector2 = $NavigationRect.shape.extents
	navigation_rect = Rect2(pos - extents, 2 * extents)
	_audio.stream = _sfx_build
	_audio.play()
	_smoke.emitting = false
	add_to_group("buildings")
	_fow_circle = FogOfWar.create_circle(32)
	_fow_circle.global_position = self.global_position


func _physics_process(delta):
	if _co is GDScriptFunctionState and _co.is_valid():
		_co = _co.resume(delta)
	elif _build_queue.size() > 0 and Global.game.wood >= 1:
		Global.game.update_wood(-1)
		_co = _build_routine().resume(delta)


func queue_units(amount: int) -> bool:
	var queued_once: bool = false
	for _i in range(amount):
		if _build_queue.size() < _queue_limit:
			queued_once = true
			_build_queue.push_back(1)
	return queued_once


func dequeue_units(amount: int) -> bool:
	var dequeued_once: bool = false
	for _i in range(amount):
		if _build_queue.size() > 0:
			dequeued_once = true
			_build_queue.pop_back()
	if _build_queue.size() == 0:
		_co = null
		_smoke.emitting = false
		_progress = 0.0
	return dequeued_once


func get_queue_size() -> int:
	return _build_queue.size()


func get_build_progress() -> float:
	return _progress / unit_build_time


func set_waypoint(pos: Vector2):
	waypoint.global_position = pos
	_harvest_on_spawn = false


func set_waypoint_harvest(pos: Vector2):
	waypoint.global_position = pos
	_harvest_on_spawn = true


func _build_routine():
	_smoke.emitting = true
	_progress = 0.0
	while true:
		var delta: float = yield()
		_progress += delta
		if _progress >= unit_build_time:
			break
	_spawn_unit()
	_build_queue.pop_front()
	_smoke.emitting = false
	_progress = 0.0


func _spawn_unit():
	var unit = Global.game.spawn_unit(position)
	if _harvest_on_spawn:
		unit.command_harvest_at(waypoint.global_position)
	else:
		var offset = Vector2(randi() % 10 - 5, randi() % 10 - 5)
		unit.command_go_to(waypoint.global_position + offset)
	_audio.stream = _sfx_spawn_unit
	_audio.play()
