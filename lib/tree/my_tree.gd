class_name MyTree
extends Area2D

var navigation_rect: Rect2
var health: int = 5

var jitter: Vector2


func _ready():
	var col = $CollisionShape2D
	jitter = Vector2(randi() % (Map.tree_jitter*2) - Map.tree_jitter, randi() % (Map.tree_jitter*2) - Map.tree_jitter)
	col.position.x += jitter.x
	col.position.y += jitter.y
	var pos: Vector2 = $NavigationRect.global_position
	var extents: Vector2 = $NavigationRect.shape.extents
	navigation_rect = Rect2(pos - extents, 2 * extents)
	add_to_group("trees")
