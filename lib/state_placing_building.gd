extends FSMState

var _sfx_forbidden := preload("res://lib/sfx/forbidden.wav")

# Virtual function. Receives events from the `_unhandled_input()` callback.
func _handle_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and !event.pressed:
		match event.button_index:
			BUTTON_LEFT:
				get_tree().set_input_as_handled()
				var game := Global.game
				if game.wood < game.building_cost:
					Global.game.play_sfx(_sfx_forbidden)
					# TODO(david): how to indicate insufficient funds??
				elif game.building_placeholder.intersecting or not Global.visible_over:
					Global.game.play_sfx(_sfx_forbidden)
				else:
					var world_pos = game.get_global_mouse_position().floor()
					game.place_building(world_pos)
					fsm.transition_to("Idle")
			BUTTON_RIGHT:
				get_tree().set_input_as_handled()
				fsm.transition_to("Idle")


# Virtual function. Corresponds to the `_process()` callback.
func _update(_delta: float) -> void:
	if Global.game.building_placeholder.intersecting or not Global.visible_over:
		Global.cursor = Global.Cursor.FORBID
	else:
		Global.cursor = Global.Cursor.HAMMER


# Virtual function. Corresponds to the `_physics_process()` callback.
func _physics_update(_delta: float) -> void:
	pass


# Virtual function. Called by the state machine upon changing the active state. The `msg` parameter
# is a dictionary with arbitrary data the state can use to initialize itself.
func _enter(_msg := {}) -> void:
	Global.game.can_select = false
	Global.game.building_placeholder.enable()


# Virtual function. Called by the state machine before changing the active state. Use this function
# to clean up the state.
func _exit() -> void:
	Global.game.can_select = true
	Global.game.building_placeholder.disable()
