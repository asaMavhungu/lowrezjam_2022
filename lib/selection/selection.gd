extends CanvasLayer

onready var _camera := $"%Camera"
onready var _selected := $"%Selected"

var _deselected: Node


func sync_with_camera(camera: Camera2D):
	var remote_transform := RemoteTransform2D.new()
	remote_transform.remote_path = _camera.get_path()
	camera.add_child(remote_transform)


func set_deselect_node(node: Node):
	_deselected = node


func select(node: Node):
	_reparent(node, _selected)


func deselect(node: Node):
	_reparent(node, _deselected)


func _reparent(child: Node, new_parent: Node):
	child.get_parent().remove_child(child)
	new_parent.add_child(child)
	child.set_owner(new_parent)
