extends Node

signal navigation_updated()
signal wood_updated(new, old)
signal building_selected(building)
signal building_deselected(building)
