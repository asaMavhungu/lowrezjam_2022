extends Camera2D

export var speed: float = 100
export var margin: int = 16

func _physics_process(delta):
	var v := Vector2.ZERO
	if Input.is_action_pressed("cam_right"):
		v.x += speed
	if Input.is_action_pressed("cam_left"):
		v.x -= speed
	if Input.is_action_pressed("cam_up"):
		v.y -= speed
	if Input.is_action_pressed("cam_down"):
		v.y += speed
	position += v * delta
	var viewport_size := get_viewport_rect().size
	position.x = clamp(round(position.x), -margin, max(Map.size.x + margin - viewport_size.x, -margin))
	position.y = clamp(round(position.y), -margin, max(Map.size.y + margin - viewport_size.y, -margin))
