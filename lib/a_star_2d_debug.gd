# source: https://github.com/uheartbeast/astar-tilemap
class_name AStar2DDebug
extends Control

var _rids := []

func _ready():
	rect_size = Map.size
	Events.connect("navigation_updated", self, "_on_navigation_updated")


func _draw():
	var astar: AStar2D = Navigate.astar
	for rid in _rids: VisualServer.free_rid(rid)
	_rids.clear()
	for point in astar.get_points():
		if astar.is_point_disabled(point): continue

		var ci_rid = VisualServer.canvas_item_create()
		_rids.push_back(ci_rid)
		VisualServer.canvas_item_set_parent(ci_rid, get_canvas_item())

		var point_position = astar.get_point_position(point)
		VisualServer.canvas_item_add_circle(ci_rid, point_position, 0.5, Color.red)
		for connected_point in astar.get_point_connections(point):
			if astar.is_point_disabled(connected_point): continue
			var connected_point_position = astar.get_point_position(connected_point)
			VisualServer.canvas_item_add_line(ci_rid, point_position, connected_point_position, Color.red, 0.25)


func _on_navigation_updated():
	update()
