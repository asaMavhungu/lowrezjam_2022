extends FSMState

var _building: Building = null
var _btn_unit_tween: SceneTreeTween = null
var _sfx_click_btn_unit := preload("res://lib/sfx/click_btn_unit.wav")
var _sfx_click_btn_unit_dequeue := preload("res://lib/sfx/click_btn_unit_dequeue.wav")
var _sfx_forbidden := preload("res://lib/sfx/forbidden.wav")
var _sfx_select_building := preload("res://lib/sfx/select_building.wav")
var _sfx_set_waypoint := preload("res://lib/sfx/set_waypoint.wav")

# Virtual function. Receives events from the `_unhandled_input()` callback.
func _handle_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and !event.pressed:
		match event.button_index:
			BUTTON_LEFT:
				fsm.transition_to("Idle")
			BUTTON_RIGHT:
				if Global.world_over is MyTree:
					_building.set_waypoint_harvest(Global.world_over.global_position)
				else:
					_building.set_waypoint(Global.game.get_global_mouse_position().floor())
				Global.game.play_sfx(_sfx_set_waypoint)


# Virtual function. Corresponds to the `_process()` callback.
func _update(_delta: float) -> void:
	if Global.world_over is MyTree:
		Global.cursor = Global.Cursor.AXE
	else:
		Global.cursor = Global.Cursor.ARROW


# Virtual function. Corresponds to the `_physics_process()` callback.
func _physics_update(_delta: float) -> void:
	pass


# Virtual function. Called by the state machine upon changing the active state. The `msg` parameter
# is a dictionary with arbitrary data the state can use to initialize itself.
func _enter(msg := {}) -> void:
	_building = msg.building
	Global.game.btn_unit.connect("pressed", self, "_on_BtnUnit_pressed")
	Selection.select(_building)
	_building.waypoint.visible = true
	Events.emit_signal("building_selected", _building)
	Global.game.play_sfx(_sfx_select_building)


# Virtual function. Called by the state machine before changing the active state. Use this function
# to clean up the state.
func _exit() -> void:
	Global.game.btn_unit.disconnect("pressed", self, "_on_BtnUnit_pressed")
	Selection.deselect(_building)
	_building.waypoint.visible = false
	Events.emit_signal("building_deselected", _building)
	_building = null


func _on_BtnUnit_pressed():
	var unit_count = 5 if Input.is_key_pressed(KEY_SHIFT) else 1
	if Input.is_action_just_released("mouse_button_right"):
		var success = _building.dequeue_units(unit_count)
		Global.game.play_sfx(_sfx_click_btn_unit_dequeue if success else _sfx_forbidden)
	else:
		var success = _building.queue_units(unit_count)
		Global.game.play_sfx(_sfx_click_btn_unit if success else _sfx_forbidden)
