extends FSMState

var _waypoint_scene := preload("res://lib/waypoint/waypoint.tscn")

var _sfx_select_unit = preload("res://lib/sfx/select_unit.wav")
var _sfx_command_unit = preload("res://lib/sfx/command_unit.wav")

var _units: Array
var _waypoints: Array


# Virtual function. Receives events from the `_unhandled_input()` callback.
func _handle_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and !event.pressed:
		match event.button_index:
			BUTTON_LEFT:
				fsm.transition_to("Idle")
			BUTTON_RIGHT:
				if Global.world_over is MyTree:
					_command_harvest_tree(Global.world_over)
				elif Global.world_over is Building:
					_command_deposit_wood(Global.world_over)
				else:
					_command_navigate_to(Global.game.get_global_mouse_position())
				Global.game.play_sfx(_sfx_command_unit)


# Virtual function. Corresponds to the `_process()` callback.
func _update(_delta: float) -> void:
	if Global.world_over is MyTree:
		Global.cursor = Global.Cursor.AXE
	elif Global.world_over is Building:
		Global.cursor = Global.Cursor.POINTER
	else:
		var mouse_world_pos := Global.game.get_global_mouse_position().floor()
		var x := mouse_world_pos.x
		var y := mouse_world_pos.y
		var world_size := Map.size
		var water_data := Map.water_data
		var i := y * world_size.x + x
		# TODO(david) perchance provide a function to check for water at pos in game.gd
		if x >= 0 and x < world_size.x and y >= 0 and y < world_size.y and water_data[i] <= 0.75 * 255:
			Global.cursor = Global.Cursor.ARROW
		else:
			Global.cursor = Global.Cursor.FORBID


# Virtual function. Corresponds to the `_physics_process()` callback.
func _physics_update(_delta: float) -> void:
	pass


# Virtual function. Called by the state machine upon changing the active state. The `msg` parameter
# is a dictionary with arbitrary data the state can use to initialize itself.
func _enter(msg := {}) -> void:
	_units = msg.units
	for unit in _units:
		unit.select()
	_update_waypoints()
	Global.game.play_sfx(_sfx_select_unit)


# Virtual function. Called by the state machine before changing the active state. Use this function
# to clean up the state.
func _exit() -> void:
	_clear_waypoints()
	for unit in _units:
		unit.deselect()


func _command_navigate_to(pos: Vector2):
	var target := pos.floor()
	# NOTE(david): navigates units into grid formation
	var unit_count := _units.size()
	if unit_count == 1:
		_units[0].command_go_to(target)
	else:
		var ts := 4 # tile size
		var l := sqrt(unit_count) * ts / 2
		var i: int = 0
		for y in range(-l, l + 1, ts):
			for x in range(-l, l + 1, ts):
				if i >= unit_count:
					break
				var offset := Vector2(x, y)
				_units[i].command_go_to(target, offset)
				i += 1
	_update_waypoints()


func _command_harvest_tree(tree: MyTree):
	for unit in _units:
		unit.command_harvest(tree)


func _command_deposit_wood(building: Building):
	for unit in _units:
		unit.command_deposit_wood(building)


func _clear_waypoints():
	for wp in _waypoints:
		Global.game.entities_box.remove_child(wp)
		wp.queue_free()
	_waypoints.clear()


func _update_waypoints():
	_clear_waypoints()
	var waypoints: Dictionary = {}
	for unit in _units:
		if unit.commanded_target:
			waypoints[unit.commanded_target_pos] = unit.commanded_target_pos
	for pos in waypoints:
		var waypoint := _waypoint_scene.instance()
		waypoint.position = pos
		Global.game.entities_box.add_child(waypoint)
		waypoint.set_owner(Global.game.entities_box)
		_waypoints.push_back(waypoint)
