extends Node

var game: Game = null

enum DebugView {
	NONE = 0,
	ASTAR,
	UNIT_NAV,
}

var debug_view = DebugView.NONE

enum Cursor {
	ARROW = 0,
	POINTER,
	AXE,
	HAMMER,
	FORBID,
}

var cursor = Cursor.ARROW

var ui_over: Control = null
var world_over: Node2D = null
var visible_over: bool = false
