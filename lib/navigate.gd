extends Node2D

var astar := AStar2D.new()
onready var astar_debug := $AStar2DDebug

var _generated_size: Vector2

func generate(size: Vector2, water_data: PoolByteArray, water_level: int):
	_generated_size = size
	# NOTE(david): generate A* grid
	astar.clear()
	astar.reserve_space(size.x * size.y)
	var tile_size := 6 # tile size
	# warning-ignore:integer_division
	var tile_offset := tile_size / 2
	for y in range(tile_offset, size.y, tile_size):
		for x in range(tile_offset, size.x, tile_size):
			var i := y * size.x + x
			if water_data[i] < water_level:
				astar.add_point(i, Vector2(x, y))
				for u in range(-1, 2):
					for v in range(-1, 2):
						if u == 0 and v == 0:
							continue;
						var neighbor_id = (u * tile_size + y) * size.x + (v * tile_size + x)
						if astar.has_point(neighbor_id):
							astar.connect_points(i, neighbor_id)


func enable_rect(rect: Rect2, enable: bool = true):
	for y in range(rect.position.y, rect.end.y):
		for x in range(rect.position.x, rect.end.x):
			var i := y * _generated_size.x + x
			if astar.has_point(i):
				astar.set_point_disabled(i, !enable)


func find_path(from: Vector2, to: Vector2) -> Array:
	# NOTE(david): build path
	var from_id := astar.get_closest_point(from)
	var to_id := astar.get_closest_point(to)
	var path = [from]
	path.append_array(astar.get_point_path(from_id, to_id))
	# NOTE(david): avoid small backtrack
	if path.size() > 2:
		var from_to_second = (from - path[2]).length_squared()
		var first_to_second = (path[1] - path[2]).length_squared()
		if from_to_second < first_to_second:
			path.remove(1)
	# NOTE(david): avoid straying too far into unpathable terrain
	var distance_to_target := (to - astar.get_point_position(to_id)).length_squared()
	var enabled_connection_count := 0
	for conn_id in astar.get_point_connections(to_id):
		if !astar.is_point_disabled(conn_id):
			enabled_connection_count += 1
	if enabled_connection_count == 8 or distance_to_target <= 3^2:
		path.push_back(to)
		# NOTE(david): avoid small backtrack
		if path.size() > 2:
			var seclast_to_last = (path[-2] - path[-1]).length_squared()
			var thirdlast_to_last = (path[-3] - path[-1]).length_squared()
			if seclast_to_last > thirdlast_to_last:
				path.remove(path.size() - 2)
	return path
